# Release Stage

> See this doc as a [visual map on Figma](https://www.figma.com/file/0pRVTOu9inH1u2hT17JsXB/Release-Product-Direction?node-id=1%3A22)

The Release stage puts what you've developed into the hands of customers. It comes after you've configured your continuous integration pipelines, configured your infrastructure, and before operating and monitoring for feedback and improvement.

See: 
 - [Release Section Direction](https://about.gitlab.com/direction/ops/#release)
 - `New` [Deployment Direction doc](https://about.gitlab.com/direction/deployment)

### Mission

Empower all teams to effectively deploy when, where, and how they want, without compromising security and support traceability naturally.

### Categories

| Category | Description | Maturity |
| ------ | ------ | ------ |
| Continuous Delivery | Deliver your changes to production with zero-touch software delivery; focus on building great software and allow GitLab CD to bring your release through your path to production for you | Complete |
| Pages | Use any static site generator to create websites that are easily managed and deployed by GitLab | Complete |
| Review Apps | Get a full production like environment for every merge request that updates on each commit. See code running and enable user acceptance testing before you merge | Complete |
| Advanced Deployments | Mitigate the risk of production deploys by deploying new production code to a small subset of your fleet and then incrementally adding more | Viable |
| Feature Flags | Feature flags enable teams to achieve CD by letting them deploy dark features to production as smaller batches for controlled testing, separating feature delivery from customer launch, and removing risk from delivery | Viable |
| Release Orchestration | Management and orchestration of releases-as-code built on intelligent notifications, scheduling of delivery and shared resources, blackout periods, relationships, parallelization, and sequencing, as well as support for integrating manual processes and interventions | Viable |

<i>See also: [Stage Direction](https://about.gitlab.com/direction/release/)</i>

### [FY22 Roadmap Highlights](https://gitlab.com/gitlab-com/Product/-/issues/1906)

| Continuous Delivery |
| ------ |
| Support DORA4 metrics in GitLab (project and group level) |
| Increase Discoverability for CD features |
| Support multi cloud deployments |

| Advanced Deployments |
| ------ |
| Advanced Deployments for AWS |
| Increase Discoverability for CD features |
| Support multi cloud deployments |

| Feature Flags |
| ------ |
| Feature Flag as issue type |
| Contextual Code References to Feature flags |

| Release Orchestration |
| --- |
| Auto Changelog |
| Group Level environments page |
| Improve Environments |


### [FY22 Q1 Goals](https://gitlab.com/groups/gitlab-org/-/epics/4696)

| Highlights |
| ------ |
| [Manual Approval Action on Pipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/35412) `Continuous Delivery` |
| [Present Lead time for MRs to be deployed to production in CI/CD dashboard - Ultimate](https://gitlab.com/gitlab-org/gitlab/-/issues/250329) `Continuous Delivery` `DORA4` |
| [Blue/Green Deployments for AWS ECS](https://gitlab.com/gitlab-org/gitlab/-/issues/226994) `Advanced Deployments` |

### [FY22 Q2 Goals](https://gitlab.com/groups/gitlab-org/-/epics/5612#dora4-metrics)

| Highlights |
| ------ |
| [Iterate Designs for DORA4 Metrics](https://gitlab.com/groups/gitlab-org/-/epics/5612#dora4-metrics) `Continuous Delivery` `DORA4` |
| [Design and Solution Validation for Environments](https://gitlab.com/groups/gitlab-org/-/epics/5304) `Release Orchestration` |
| [Generative Research for Multi Cloud Deployments](https://gitlab.com/groups/gitlab-org/-/epics/5612#research-man_detective) `Advanced Deployments` |


### UX Pages

[CI/CD UX Team](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/)  

[Release UX Team](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/release/)  
[Release Management UX](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/release/release-management/#performance-indicators)  

[Package UX Team](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/package/)  
[Verify UX Team](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/verify/)  
