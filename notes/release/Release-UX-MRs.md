# A guide for better UX MR reviews - or “how to make your designer happier”

This is a short guide with suggestions to facilitate UX MR reviews. These are all optional, and based on my experiences in the Release group 🚀

Feel free to ping me (@dfosco) on Slack if you'd like to contribute to these!
### Add the designer as reviewer and the UX label

Don't forget to add the relevant designer as reviewer for your MR, and to add the ~UX label
### Consider making the UX review blocking if it's essential to this MR

For larger or more complex UI changes, consider having the UX review as a blocker for the MR.

To avoid having a third contributor merge the MR immediately after their code review, you can leave the MR as `Draft:` until the UX review has been made.
### Create the MR from the issue

Consider creating your MR directly from the issue, so it inherits all the metadata and relevant labels from the issue it's resolving.

### Add current vs. proposed changes on your screenshots or screen recordings

If you can, show the current state of the application vs the change being introduced in two separate screenshots or videos. 

Especially for simple UI changes, having detailed screenshots or videos on the MR can make reviewing and approval much faster!

### Document alternate UI states 

Does the UI contain tooltips on hover? Does the layout seems to break on smaller viewports? What does the empty state for the feature in the MR looks like?

Especially if one of these aspects is key to the changes being introduced by the MR, adding screenshots or videos for them can also help speed up UX reviews!

### Providing setup instructions

Consider adding instructions for how to turn on feature flags, setting up runners, or even an example `.gitlab-ci.yml` template if that would be helpful for testing the changes.

Check out this example from https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78397:

1. Make sure to turn on the `new_environments_table` feature flag. On the GitLab repository, run:

```
cd ./bin 
./rails console
Feature.enable(:new_environments_table) //turns on the feature flag
Feature.disable(:new_environments_table) //turns off the feature flag, takes some time to kick in
```

2. Enable runners in your GDK/Gitpod -- [see instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/gitpod.md#enable-runners)
3. Then add the following `.gitlab-ci.yml` to a project for some deployments:

```
stages:
    - deploy
    - stop
image: alpine:latest

deploy-prod:
    stage: deploy
    script:
        - sleep 6000
        - echo "deploying prod"
    environment:
        name: production

...
```

4. Run a pipeline and check out the environments page!