# 1

> When I am requested from an auditor to sample changes to production I want to easily and naturally provide the source of the change so I can adequately respond to an audit	

**When I am requested to sample changes to production for an audit** 

**I want to easily provide the source of the change** 

**so I can adequately respond to an audit**

> When I am requested to sample changes to production for an audit I want to easily provide the source of the change so I can adequately respond to the audit

# 2

> When a compliance check is run and the compliance team wants to confirm we have records of production I want to be able to minimize the disruption to the internal teams by empowering the compliance team to self-serve these changes without giving access to the source code so I can adequately respond to an audit	

**When I am requested to confirm records of production for an audit** 

**I want to enable the compliance team to self-serve these changes without giving access to the source code** 

**so I can adequately respond to the audit, while minimizing the disruption to internal teams**

> When I am requested to confirm records of production for an audit I want to enable the compliance team to self-serve these changes without giving access to the source code so I can adequately respond to the audit, while minimizing disruption to internal teams

# 3

> When an auditor is asking for proof of separation of duties in my changes to productions I want to be able to show who made the change and who approved the change to production so I can adequately respond to an audit	

**When I am requested proof of separation of duties in production changes for an audit** 

**I want to be able to show who made the change and who approved the change to production** 

**so I can adequately respond to the audit**

> When I am requested proof of separation of duties in production changes for an audit I want to be able to show who made the change and who approved the change to production so I can adequately respond to the audit