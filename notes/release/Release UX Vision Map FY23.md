# [DRAFT] Release UX Vision Map FY23

This document outlines the expected UX to be designed and/or implemented in Release on FY23. It's still a draft, and is meant to inform product planning and help drive the conversation.

The main UX items currently in our roadmap are listed below, roughly in order of execution:

| Effort | Status | Link |
| ------ | ------ | ------ |
| Environments Page re-design | `implementation` | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/6938) |
| Deployment Approvals | `implementation` | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/6832) |
| Cross-project Environments | `design`  | [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/241506) |
| Connection between releases and deployments | `planning` | [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/332103) |
| Connection between packages and releases | `planning` | [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/332103) |
| Recurring Customer Interviews for Release | `research` `planning` | [Issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/108) |

The existing [FY22 UX Vision map](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/release/release-management/#ux-vision-map---whats-next) is an important source of context to keep adding items to this list. 
