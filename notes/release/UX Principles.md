**We support all users from beginners to experts**

We believe that GitLab software should be unintimidating and accessible for a beginner, without oversimplifying important features for advanced users. We stay with users every step of the way to help them learn fast as a beginner and then become an expert over time,

**We’re building one product, together**

We’re highly focused on ensuring that no matter how big our product gets, the entire experience stays cohesive, and interconnected.

**We’re humble facilitators of user experience design**

Everyone is a designer; everyone can contribute. We are not egotistical, moody experts who alone hold the keys to user delight. We encourage Product Managers, Engineers, and the wider GitLab community to contribute to creating an exceptional user experience.

**We look for small changes and big impacts**

Sometimes the simplest, most boring solution is what is needed to make users successful. We want our UI to stay out of the user’s way. We work iteratively to make modest but valuable changes that make users more productive, faster, and better at accomplishing their tasks.

**We’re informed by empathy**

We’re human, and we design for humans, so we strive for understanding, self-awareness and connection. We are quirky, and we introduce our quirks into designs when appropriate.

**When we find problems that are simple to fix, we are empowered to make those changes ourselves**

If a change will take you less than 15 minutes to make (for example, a minor change to our website or microcopy in the product), then start with a MR instead of an issue. By making the change yourself, you are taking immediate action to improve our product, and you might learn a new skill, too! If it seems simple, but you have questions, remember that there are people who can help you with code changes both in the UX department and across the company (even Sid is willing to help, if you need it.)