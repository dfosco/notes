## 🤝 Collaboration

- **Kindness**
- **Share**
- **Negative Feedback is 1-1**
- **Say Thanks**
- **Give Feedback effectively**
- **Get to know each other**
- **Don’t pull rank**
- **Assume positive intent**
- **Address behavior, but don’t label people**
- **Say sorry**
- **No ego**
- **See others succeed**
- **Don’t let each other fail**
- **People are not their work**
- **Do it yourself**
- **Blameless problem solving**
- **Short toes**
- **It’s impossible to know everything**
- **Collaboration is not consensus**
- **Collaboration is not playing politics**

## 📈 Results

- **Measure results not hours** — “if you are working too many hours, talk to your manager to discuss solutions”
- **Dogfooding**
- **Give agency** — “We give people the agency to focus on what they think is most beneficial. If a meeting doesn’t seem interesting and someone’s active participation is not critical [...] they can always opt to not attend”
- **Write promises down** — “Agree in writing on measurable goals”
- **Growth mindset**
- **Global Optimization** — “you do what’s best for the organization as a whole. Don’t optimize for the goals of your team when it negatively impacts the goals of other teams, our users, and/or the company”
- **Tenacity** — “is the ability to display commitment to what you believe in. You keep picking yourself up, dusting yourself off, and quickly get going again having learned a little more.
- **Ownership**
- **Sense of Urgency** — “time gained or lost has compounding effects. Try to get the results as fast as possible, but without compromising on our other values and ways we communicate”
- **Ambitious**
- **Perseverance** — “We value the ability to maintain focus and motivation when work is tough and asking for help when needed”
- **Bias for Action** — “Decisions should be thoughtful, but delivering fast results requires the fearless acceptance of occasionally making mistakes; our bias for action also allows us to course correct quickly”
- **Accepting Uncertainty**
- **Customer Results** — “Customer results are more important than:
    - What we plan to make
    - Large customers
    - What customers ask for
    - Our existing scope
    - Our assumptions
    - What we control

## ⏱️ Efficiency

- **Write things down**
- **Boring solutions**
- **Self-service and self-learning**
- **Efficiency for the right group**
- **Be respectful of others’ time**
- **Spend company money like it’s your own**
- **Frugality** — “Accomplish more with less. Constraints breed resourcefulness, self-sufficiency, and invention. There are no extra points for growing headcount, budget size, or fixed expense.”
- **Conversational Development** — “Shorten the conversation cycle; Thread the conversation through all stages; Open conversations without consensus; Result oriented conversation”
- **Short Verbal Answers**
- **Keep broadcasts short**
- **Managers of one**
- **Freedom and responsibility over rigidity**
- **Accept mistakes**
- **Move fast by shipping the minimal viable change**
- **Embrace change**

## 🌐 Diversity, Inclusion & Belonging

- **Bias towards asynchronous communication** — “[...] this shows care for those who may not be in the same time-zone, are traveling outside of their usual time zone, or are structuring their day around pressing commitments at home or in their community”
- **Understanding the impact of microaggressions**
- **Reach across company departments**
- **Make family welcome**
- **Shift working hours for a cause**
- **Be a mentor**
- **Culture fit is a bad excuse**
- **Religion and politics at work**
- **Quirkiness**
- **Building a safe community**
- **Unconscious Bias**
- **Inclusive benefits**
- **Inclusive language & pronouns**
- **Inclusive Interviewing**
- **Inclusive Meetings**
- **See something, say something**
- **Neurodiversity**
- **Family and friends first, work second**

## 👣 Iteration

- **Don’t wait**
- **Set a due date**
- **Cleanup over sign-off**
- **Reduce cycle time**
- **Work as part of the community**
- **Minimal Viable Change (MVC)**
- **Make a proposal**
- **Everything is in draft**
- **Under construction**
- **Low level of shame**
- **Cultural lens**
- **Focus on improvement**
- **Do things that don’t scale**
- **Make two-way door decisions**
- **Changing proposals isn’t iteration**
- **Embracing iteration**
- **Make small merge requests**
- **Always iterate deliberately**

## 👁️ Transparency

- **Public by Default**
- **Not public**
- **Directness**
- **Articulate when you change your mind** — “articulating that an *earlier* stance is not your *current* stance provides clarity to others and encourages data-driven decision making”
- **Surface issues constructively**
- **Anyone and anything can be questioned** — “any past decisions and guidelines are open to questioning as long as you act in accordance with them until they are changed”
- **Disagree, commit, and disagree**
- **Transparency is only a value if you do it when it is hard**
- **Single source of truth**
- **Findability**
- **Find the limit** — “we accept if we’re sometimes publicizing information that should have remained confidential in retrospect. Most companies become non-transparent over time because they don’t accept any mistakes”
- **Say why, not just what**
- **Reproducibility**
- **Accountability**

## Five Dysfunctions

- Absence of trust
- Fear of conflict
- Lack of commitment
- Avoidance of accountability
- Inattention to results