# Goal: Become an active contributor for the Release product and Pajamas design system

### Quarterly goals (Q1FY22 – Feb/Mar/Apr)

- [ ] Complete the company onboarding and get started with contributing to the Release stage group by participating on your first milestone as UX DRI.
- [ ] Review the Release category direction pages to gain deeper knowledge of vision and maturity plan and discuss with PM during 1:1s.
- [ ] Become familiar with the Performance Assessment process at GitLab.
- [ ] (Stretch): Get started with contributing to Pajamas.

### Yearly Goals (2021)

#### Become a champion for the values and collaboration model at GitLab
**Learn the collaboration model and make contributions that drive the GitLab values forward**

- [ ] Contribute to the GitLab Handbook with at least 1 MR per month
- [ ] Join one Diversity, Inclusion and Belonging initiative on GitLab
- [ ] Collaborate with peers on other groups by having at least 2 coffee chats / pairings per month

#### Make an impact in the Release product
**Understand the product, help define the vision and become the key driver of UX in the release group**

- [ ] Map the products and features in Release to drive the design vision for the group
- [ ] Define an intentional design process for the Release team  
- [ ] Help the CI/CD UX Team develop a stronger continuous research practice for our group

#### Become an active contributor to the GitLab design system
**Contribute to the components, tooling and processes for the GitLab design system**

- [ ] Contribute to the design system from within Release -- integrating components and offering feedback
- [ ] Contribute design and code to a Pajamas component
- [ ] Contribute to component documentation / tooling / processes

---

#### [Responsibilities](https://about.gitlab.com/job-families/engineering/product-designer/#product-designer)

- [ ] Have working knowledge of the product area to which you are assigned and proactively learn other product areas.
- [ ] Independently conduct solution validation with minimal guidance from your Product Design Manager and incorporate insights into design decisions to fulfill user and business needs.
- [ ] Create deliverables for your product area (for example: competitive analyses, low fidelity wireframes, high fidelity mockups, prototypes, journey maps, storyboards, personas, design vision, strategic planning, etc.) that help define the vision and execution of solving real user problems through the user experience.
- [ ] Communicate the results of UX activities with a strong point of view within your stage group to the UX department, cross-functional partners within your product area, and other interested GitLab team-members using clear language that simplifies complexity.
- [ ] Proactively identify both small and large usability issues within your product area, and help influence your Product Design Manager and Product Manager to prioritize them.
- [ ] Lead and coach iteration of design work. Take part in the monthly release process by breaking down the designs to fit release cadence, reviewing and approving merge requests submitted by developers.
- [ ] Participate in Design Reviews, and model best practices for giving and receving feedback
- [ ] Identify and influence the prioritization of UX debt.
- [ ] Actively contribute to the Pajamas Design System, help determine whether components are single-use or multi-use, and provide recommendations to designers regarding new component requests.
- [ ] Engage in social media efforts, including writing blog articles, giving talks, and responding on Twitter, as appropriate.
- [ ] Interview potential UX candidates.
- [ ] Support your Product Design Manager and Product Manager in identifying dependencies between stages and advocating for cross-stage collaboration when needed.
- [ ] Mentor other members of the UX department, both inside and outside of your product area.

#### [Values Competencies](https://about.gitlab.com/handbook/competencies/#values-competencies)

- [ ] **Collaboration** – Models collaborative behavior for fellow team members and others within the group.
- [ ] **Results**	– Models a sense of urgency and commitment to deliver results.
- [ ] **Efficiency** –	Models a culture of efficiency within the team where people make good, timely decisions using available data and assessing multiple alternatives. Models using boring solutions for increasing the speed of innovation for our organization and product.
- [ ] **Diversity, Inclusion & Belonging** – Actively aware of how bias or exclusion might occur on a team and helps to facilitate a team environment where team members belong and feel safe. Models empathy with their interactions with customers and cross functional team members.
- [ ] **Iteration** – Independently balances short term gains and long term benefit. Identifies opportunities to model the processes around iteration. If a colleague privately asks a question, asks the question in a public channel (if they don't know the answer). Models a growth-mindset by exposing the limits of your knowledge and demonstrating the machine you've built to fill those gaps.
- [ ] **Transparency** – Continually surfaces improvements across their functional area of expertise. They share feedback with others and understand how to disagree and commit to final solutions. They model what it means to be as open as possible.

#### [Remote Work Competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies)

- [ ] **Manager of one** – Develops their daily priorities to achieve goals. 
- [ ] **Effective communication** – Uses asynchronous communication as the starting point and stays open and transparent by communicating via text through public issues, merge requests, and Slack channels (over DMs). Places an emphasis on ensuring that conclusions of offline conversations are written down ensuring a Single Source of Truth and producing Video when necessary.
- [ ] **Handbook first** – Actively contributes to the handbook. Everything starts with a merge request. Provides links information from the handbook when answering questions and if the information doesn't exist in the handbook, then the team member adds it.
- [ ] **Uses GitLab** – Knows when and how to open, comment, close, and move on issues while utilizing an issue board. Understands and knows how to open and submit a merge request. Models how and when to use epics. Uses GitLab for managing day-to-day work.